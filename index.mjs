import * as utils from 'test-utils';
import util from 'util';

const params = {password: 'df345w43jkj3443d'};

utils.runMePlease(params, (response) => {
	console.log(response);
});

const runPromise = util.promisify(utils.runMePlease);

(async function() {
try {
  const answer = await runPromise(params)
  console.log(answer);
} catch(e) {
  console.log(e);
}
})()